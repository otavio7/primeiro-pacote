<?php

namespace Otavio7\PrimeiroPacote;

    describe( 'Confere pacote', function(){

        $this->ex = null;

        beforeAll( function() {
            $this->ex = new Exemplo();
        });

        it( 'Testa se nome retorna o primeiro pacote', function(){
            expect($this->ex->nome())->toContain("primeiro pacote");
        });


    } );
    ?>